FROM ubuntu:bionic
ENV DEBIAN_FRONTENT=noninteractive
RUN apt-get update
RUN apt-get install --yes curl build-essential
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install --yes nodejs
RUN adduser --gecos "Service user account" --disabled-password user
RUN install --owner user --directory /app
USER user
WORKDIR /app
COPY package.json /app
COPY *.js /app
ENV PORT=8080
EXPOSE 8080
RUN npm install
CMD ["npm","start"]
